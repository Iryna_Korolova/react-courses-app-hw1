import './Button.css';

export default function Button({ buttonText, buttonType = 'button', onClick }) {
	return (
		<button className='btn' onClick={onClick} type={buttonType}>
			{buttonText}
		</button>
	);
}
