import './Input.css';

import { useState, useEffect } from 'react';

export default function Input({
	onChange,
	inputType = 'text',
	inputName,
	placeholdetText,
	labelText,
}) {
	const [inputId, setInputId] = useState();
	useEffect(() => {
		setInputId(Date.now());
	}, []);

	return (
		<>
			<label htmlFor={inputId}>{labelText}</label>
			<input
				className='input'
				type={inputType}
				id={inputId}
				name={inputName}
				placeholder={placeholdetText}
				onChange={onChange}
			></input>
		</>
	);
}
