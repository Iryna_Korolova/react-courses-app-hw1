import { useState } from 'react';

import { mockedCoursesList } from './../../constants';

import CourseCard from './CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import CreateCourse from '../CreateCourse/CreateCourse';

export default function Courses() {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [searchValue, setSearchValue] = useState('');
	const [showCreateCourse, setShowCreateCourse] = useState(true);

	return (
		<main>
			{showCreateCourse ? (
				<div className='container'>
					<div className='searchbar-wrap'>
						<SearchBar setSearchValue={setSearchValue}></SearchBar>
						<Button
							buttonText='Add new course'
							onClick={() => setShowCreateCourse(!showCreateCourse)}
						></Button>
					</div>
					{courses
						.filter((course) =>
							`${course.title} ${course.id}`
								.toLocaleLowerCase()
								.includes(searchValue)
						)
						.map((course) => (
							<CourseCard key={course.id} course={course}></CourseCard>
						))}
				</div>
			) : (
				<CreateCourse
					setCourses={setCourses}
					setShowCreateCourse={setShowCreateCourse}
				/>
			)}
		</main>
	);
}
