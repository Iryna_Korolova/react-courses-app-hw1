import './SearchBar.css';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

export default function Searchbar({ setSearchValue }) {
	function onSubmit(event) {
		event.preventDefault();
		setSearchValue(event.target.search.value.trim().toLocaleLowerCase());
	}
	return (
		<form className='searchbar' onSubmit={onSubmit}>
			<Input
				placeholdetText='Enter course name...'
				inputType='search'
				inputName='search'
			/>
			<Button buttonText='Search' buttonType='submit' />
		</form>
	);
}
