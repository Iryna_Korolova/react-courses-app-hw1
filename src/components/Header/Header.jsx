import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.css';

export default function Header({ children }) {
	return (
		<header className='header container'>
			<div className='header-inner'>
				<Logo />
				<div className='header-inner'>
					<h3 className='header-heading'>John Doe</h3>
					<Button buttonText='Logout' />
				</div>
			</div>
		</header>
	);
}
