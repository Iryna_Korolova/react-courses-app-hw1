export default function Logo() {
	return (
		<img
			className='logo'
			src={
				'https://seeklogo.com/images/E/education-logo-89FBCEFA73-seeklogo.com.png'
			}
			width={80}
			height={60}
			alt='logo'
		/>
	);
}
